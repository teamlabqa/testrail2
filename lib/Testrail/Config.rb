# encoding: utf-8
require 'active_support/configurable'

module Testrail
  attr_reader :config

  def self.configure(&block)
    @config ||= Config.new
    yield @config if block_given?
  end

  def self.config
    @config ||= Config.new
  end

  class Config
    include ActiveSupport::Configurable

    config_accessor :server, :api_path, :username, :password, :headers

    def initialize
      default_configuration
    end

    def default_configuration
      self.server = 'https://example.testrail.com'
      self.api_path = '/index.php?/api/v2/'
      self.username = 'user@example.com'
      self.password = 'p@ssword'
      self.headers = { 'content-type' => 'application/json' }
    end
  end
end
