module Testrail
  def self.command
    @commands ||= Testrail::Commands.new
  end

  class Commands
    def initialize
      @request = Testrail::Request
    end

    # region CASES
    def get_case(case_id, opts = {})
      @request.get('get_case', { case_id: case_id }, opts)
    end

    def get_cases(project_id, suite_id, section_id, opts = {})
      @request.get('get_cases', { project_id: project_id, suite_id: suite_id, section_id: section_id }, opts)
    end

    def add_case(section_id, title, opts = {})
      @request.post('add_case', { section_id: section_id }, opts.merge(title: title))
    end

    def update_case(case_id, opts = {})
      @request.post('update_case', { case_id: case_id }, opts)
    end

    def delete_case(case_id, opts = {})
      @request.post('delete_case', { case_id: case_id }, opts)
    end
    # endregion

    # region CASE FIELDS
    def get_case_fields(opts = {})
      @request.get('get_case_fields', opts)
    end
    # endregion

    # region CASE TYPES
    def get_case_types(opts = {})
      @request.get('get_case_types', opts)
    end
    # endregion

    # region MILESTONES
    def get_milestone(milestone_id, opts = {})
      @request.get('get_milestone', { milestone_id: milestone_id }, opts)
    end

    def get_milestones(project_id, opts = {})
      @request.get('get_milestones', { project_id: project_id }, opts)
    end

    def add_milestone(project_id, name, opts = {})
      @request.post('add_milestone', { project_id: project_id }, opts.merge(name: name))
    end

    def update_milestone(milestone_id, opts = {})
      @request.post('update_milestone', { milestone_id: milestone_id }, opts)
    end

    def delete_milestone(milestone_id, opts = {})
      @request.post('delete_milestone', { milestone_id: milestone_id }, opts)
    end
    # endregion

    # region PLANS
    def get_plan(plan_id, opts = {})
      @request.get('get_plan', { plan_id: plan_id }, opts)
    end

    def get_plans(project_id, suite_id, section_id, opts = {})
      @request.get('get_plans', { project_id: project_id, suite_id: suite_id, section_id: section_id }, opts)
    end

    def add_plan(project_id, name, opts = {})
      @request.post('add_plan', { project_id: project_id }, opts.merge(name: name))
    end

    def add_plan_entry(plan_id, suite_id, opts = {})
      @request.post('add_plan_entry', { plan_id: plan_id }, opts.merge(suite_id: suite_id))
    end

    def update_plan(plan_id, opts = {})
      @request.post('update_plan', { plan_id: plan_id }, opts)
    end

    def update_plan_entry(plan_id, entry_id, opts = {})
      @request.post('update_plan', { plan_id: plan_id, entry_id: entry_id }, opts)
    end

    def close_plan(plan_id, opts = {})
      @request.post('delete_plan', { plan_id: plan_id }, opts)
    end

    def delete_plan(plan_id, opts = {})
      @request.post('delete_plan', { plan_id: plan_id }, opts)
    end

    def delete_plan_entry(plan_id, entry_id, opts = {})
      @request.post('delete_plan_entry', { plan_id: plan_id, entry_id: entry_id }, opts)
    end
    # endregion

    # region PRIORITIES
    def get_priorities(opts = {})
      @request.get('get_priorities', opts)
    end
    # endregion

    # region PROJECTS
    def get_project(project_id, opts = {})
      @request.get('get_project', { project_id: project_id }, opts)
    end

    def get_projects(opts = {})
      @request.get('get_projects', opts)
    end

    def add_project(opts = {})
      @request.post('add_project', opts)
    end

    def update_project(project_id, opts = {})
      @request.post('update_project', { project_id: project_id }, opts)
    end

    def delete_project(project_id, opts = {})
      @request.post('delete_project', { project_id: project_id }, opts)
    end
    # endregion

    # region RESULTS
    def get_results(test_id, limit = nil, opts = {})
      @request.get('get_results', { test_id: test_id }.merge(limit.nil? ? {} : { limit => limit }), opts)
    end

    def get_results_for_case(run_id, case_id, limit = nil, opts = {})
      @request.get('get_results_for_case', { run_id: run_id, case_id: case_id }.merge(limit.nil? ? {} : { limit => limit }), opts)
    end

    def add_result(test_id, opts = {})
      @request.post('add_result', { test_id: test_id }, opts)
    end

    def add_result_for_case(run_id, case_id, opts = {})
      @request.post('add_result_for_case', { run_id: run_id, case_id: case_id }, opts)
    end
    # endregion

    # region RESULT FIELDS
    def get_result_fields(opts = {})
      @request.get('get_result_fields', opts)
    end
    # endregion

    # region RUNS
    def get_run(run_id, opts = {})
      @request.get('get_run', { run_id: run_id }, opts)
    end

    def get_runs(project_id, opts = {})
      @request.get('get_runs', { project_id: project_id }, opts)
    end

    def add_run(project_id, opts = {})
      @request.post('add_run', { project_id: project_id }, opts)
    end

    def close_run(run_id, opts = {})
      @request.post('close_run', { run_id: run_id }, opts)
    end
    # endregion

    # region SECTIONS
    def get_section(section_id, opts = {})
      @request.get('get_section', { section_id: section_id }, opts)
    end

    def get_sections(project_id, suite_id, opts = {})
      @request.get('get_sections', { project_id: project_id, suite_id: suite_id }, opts)
    end

    def add_section(project_id, suite_id, name, parent_section_id = nil, opts = {})
      @request.post('add_section', { project_id: project_id }, opts.merge(name: name, suite_id: suite_id, parent_id: parent_section_id))
    end

    def update_section(section_id, opts = {})
      @request.post('update_section', { section_id: section_id }, opts)
    end

    def delete_section(section_id, opts = {})
      @request.post('delete_section', { section_id: section_id }, opts)
    end
    # endregion

    # region STATUSES
    def get_statuses(opts = {})
      @request.get('get_statuses', opts)
    end
    # endregion

    # region SUITES
    def get_suite(suite_id, opts = {})
      @request.get('get_suite', { suite_id: suite_id }, opts)
    end

    def get_suites(project_id, opts = {})
      @request.get('get_suites', { project_id: project_id }, opts)
    end

    def add_suite(project_id, opts = {})
      @request.post('add_suite', { project_id: project_id }, opts)
    end

    def update_suite(suite_id, opts = {})
      @request.post('update_suite', { suite_id: suite_id }, opts)
    end

    def delete_suite(suite_id, opts = {})
      @request.post('delete_suite', { suite_id: suite_id }, opts)
    end
    # endregion

    # region TESTS
    def get_test(test_id, opts = {})
      @request.get('get_test', { test_id: test_id }, opts)
    end

    def get_tests(run_id, opts = {})
      @request.get('get_tests', { run_id: run_id }, opts)
    end
    # endregion

    # region USERS
    def get_user(user_id, opts = {})
      @request.get('get_user', { user_id => user_id }, opts)
    end

    def get_users(opts = {})
      @request.get('get_users', opts)
    end
    # endregion
  end
end
